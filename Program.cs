﻿using System;

namespace EvenDigitsOnly
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        bool evenDigitsOnly(int n) {
            string s = Convert.ToString(n);
            
            for(int i = 0; i < s.Length; i ++)
            {
                int c = Convert.ToInt32(s[i]);
                if( c % 2 != 0){
                    return false;
                }
            }
            return true;
        }

    }
}
